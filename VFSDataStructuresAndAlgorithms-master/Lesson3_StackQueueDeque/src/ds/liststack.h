//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_LISTSTACK_H_
#define _VFS_LISTSTACK_H_

//======================================================================

#include "adt/stack.h"
#include "arraylist.h"
#include "linkedlist.h"
#include <iostream>
using namespace std;

//======================================================================

template <class T>
class ListStack: public Stack<T>
{
public:

    //-----------------------------------------------------------
    // Constructor
#if !defined USE_LINKEDLIST 
    ListStack( uint32_t const maxSize ):
        mList( maxSize )
#else
    ListStack( uint32_t const maxSize )
#endif
    {
    }

    //-----------------------------------------------------------
    // Destructor
    virtual ~ListStack()
    {
    }

    //-----------------------------------------------------------
    // Push entry to the top of the stack.
    virtual bool Push( const T& entry ) override
    {
		if(mList.Append(entry))
		{
			return true;
		}

        return false;
    }

    //-----------------------------------------------------------
    // Copy the next entry to be popped.
    virtual bool Peek( T& entry ) const override
    {
		if (mList.At(entry, mList.Size() - 1)) 
		{
			return true;
		}

        return false;
    }

    //-----------------------------------------------------------
    // Pop entry from the top of the stack.
    virtual bool Pop() override
    {
		if (mList.Remove(mList.Size() - 1)) 
		{
			return true;
		}
        return false;
    }

    //-----------------------------------------------------------
    // Return current size of stack.
    virtual uint32_t Size() const override
    {
        return mList.Size();
    }

    //-----------------------------------------------------------
    // Dump contents to stdout.
    virtual void Dump() const override
    {
		T entry;
		for (int i = 0; i < mList.Size(); i++)
		{
			mList.At(entry, i);
			cout << entry << endl;
		}
    }

protected:

#if defined USE_LINKEDLIST
    LinkedList<T> mList;
#else
    ArrayList<T> mList;
#endif
};

//======================================================================

#endif

//======================================================================
