//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_RECTF_H_
#define _VFS_RECTF_H_

//======================================================================

#include "point2f.h"
#include <algorithm>

//======================================================================

class RectF
{
public:

    // Constructor, default to invalid rectangle.
    inline RectF( const Point2F& _min = Point2F( FLT_MAX, FLT_MAX ), const Point2F& _max = Point2F( -FLT_MAX, -FLT_MAX ) ):
        min( _min ),
        max( _max )
    {
    }

    // Expand this rectangle to include the supplied point.
    inline void Union( const Point2F& point )
    {
        min.x = std::min( min.x, point.x );
        min.y = std::min( min.y, point.y );
        max.x = std::max( max.x, point.x );
        max.y = std::max( max.y, point.y );
    }

    // Returns true if the supplied point is inside this rectangle.
    inline bool Contains( const Point2F& point ) const
    {
        if (min.x <= point.x && min.y <= point.y &&
            max.x >= point.x && max.y >= point.y)
            return true;
        return false;
    }

    // Returns true if the supplied rectangle and this rectangle overlap.
    inline bool Overlaps( const RectF& rect ) const
    {
        if (rect.min.x > max.x || rect.min.y > max.y ||
            rect.max.x < min.x || rect.max.y < min.y)
            return false;
        return true;
    }

    Point2F min;
    Point2F max;
};

//======================================================================

#endif

//======================================================================
