//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_RECTQUADTREE_H_
#define _VFS_RECTQUADTREE_H_

//======================================================================

#include <vector>
#include <stdint.h>
#include <inttypes.h>
#include "rectf.h"

//======================================================================

// QuadTree of points.
class PointQuadTree
{
public:

    // X-tors.
    PointQuadTree();
    ~PointQuadTree();

    // Build QuadTree for points.
    void Build( const RectF& bounds, const std::vector<Point2F>& points, uint32_t const depth );

    // Dump Graphviz dot file for tree.
    void Dump( const std::string& fileName ) const;

    // Fill vector with points that are contained by the supplied rectangle, return the number
    // of point-rectangle tests.
    uint32_t FindPoints( std::vector<Point2F>& points, const RectF& rect ) const;

private:

    // Node base type, every node has a bounding rectangle and
    // whether or not it is a leaf or branch node.
    struct Node
    {
        inline Node( const RectF& rect, bool const isLeaf ):
            mRect( rect ),
            mIsLeaf( isLeaf )
        {
        }

        RectF const mRect;
        bool const mIsLeaf;
    };

    // Branch node, points to 4 child nodes.
    struct BranchNode: public Node
    {
        inline BranchNode( const RectF& rect ):
            Node( rect, false )
        {
            mChildren[0] = nullptr;
            mChildren[1] = nullptr;
            mChildren[2] = nullptr;
            mChildren[3] = nullptr;
        }

        Node* mChildren[4];
    };

    // Leaf node, contains vector of points.
    struct LeafNode: public Node
    {
        inline LeafNode( const RectF& rect ):
            Node( rect, true )
        {
        }

        std::vector<Point2F> mPoints;
    };

    // Recursively build tree using supplied vector of points.
    Node* BuildNode( const RectF& bounds, const std::vector<Point2F>& points, uint32_t const curDepth, uint32_t const maxDepth );

    // Recursively delete nodes.
    void DeleteNode( Node* const node );

    // Recursively dump tree in Graphiz Dot format.
    void DumpNode( FILE* const fh, const Node* const node, const Node* const parent ) const;

    // Recursively find points contained by the test rectangle.
    void FindPointsRecursive( std::vector<Point2F>& points, uint32_t& numTests, const RectF& rect, const Node* const node ) const;
    void FindPointsDFS( std::vector<Point2F>& points, uint32_t& numTests, const RectF& rect, const Node* const node ) const;
    void FindPointsBFS( std::vector<Point2F>& points, uint32_t& numTests, const RectF& rect, const Node* const node ) const;

    Node* mRootNode;
};

//======================================================================

#endif

//======================================================================
