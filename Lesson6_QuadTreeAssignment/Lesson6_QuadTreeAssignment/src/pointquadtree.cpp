//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#include "pointquadtree.h"

//=======================================================================

PointQuadTree::PointQuadTree():
    mRootNode( nullptr )
{
}

//=======================================================================

PointQuadTree::~PointQuadTree()
{
    // Use recursion to delete tree nodes.
    //DeleteNode( mRootNode );
}

//=======================================================================

void PointQuadTree::Build( const RectF& bounds, const std::vector<Point2F>& points, uint32_t const depth )
{
    // Use recursion to build tree using supplied rectangles.
    mRootNode = BuildNode( bounds, points, 1, depth );
}

//=======================================================================

void PointQuadTree::Dump( const std::string& fileName ) const
{
    // Use recursion to dump tree to Graphviz dot file.
    FILE* const fh = fopen( fileName.c_str(), "wb" );
    if (fh != nullptr)
    {
        fprintf( fh, "digraph D {\n" );
        DumpNode( fh, mRootNode, nullptr );
        fprintf( fh, "}\n" );
        fclose( fh );
    }
}

//=======================================================================

uint32_t PointQuadTree::FindPoints( std::vector<Point2F>& points, const RectF& rect ) const
{
    // Use recursion to collect points in tree that are contained by the
    // supplied rectangle.
    uint32_t numTests = 0;
    FindPointsRecursive( points, numTests, rect, mRootNode );
    return numTests;
}

//=======================================================================

PointQuadTree::Node* PointQuadTree::BuildNode( const RectF& bounds, const std::vector<Point2F>& points, uint32_t const curDepth, uint32_t const maxDepth )
{
    // If we have reached the requested tree depth build a leaf node,
    // otherwise build a branch node and recurse.
    if (curDepth < maxDepth)
    {
        // Alloc branch node.
        BranchNode* branchNode = new BranchNode( bounds );
        
        // Calc half size of rectangle.
        Point2F const halfLen = Point2F( (bounds.max.x - bounds.min.x) * 0.5f,
                                         (bounds.max.y - bounds.min.y) * 0.5f );
        // Calc quadrants.
        // UpperLeft
        RectF const upperLeft = RectF( Point2F( bounds.min.x, bounds.min.y ), 
                                       Point2F( bounds.min.x + halfLen.x, bounds.min.y + halfLen.y ) );
        // UpperRight
        RectF const upperRight = RectF( Point2F( bounds.min.x + halfLen.x, bounds.min.y ), 
                                        Point2F( bounds.max.x, bounds.min.y + halfLen.y ) );
        // LowerLeft
        RectF const lowerLeft = RectF( Point2F( bounds.min.x, bounds.min.y + halfLen.y ), 
                                       Point2F( bounds.min.x + halfLen.x, bounds.max.y ) );
        // LowerRight
        RectF const lowerRight = RectF( Point2F( bounds.min.x + halfLen.x, bounds.min.y + halfLen.y ), 
                                        Point2F( bounds.max.x, bounds.max.y ) );

        // Build child nodes recursively.
        branchNode->mChildren[0] = BuildNode( upperLeft, points, curDepth + 1, maxDepth );
        branchNode->mChildren[1] = BuildNode( upperRight, points, curDepth + 1, maxDepth );
        branchNode->mChildren[2] = BuildNode( lowerLeft, points, curDepth + 1, maxDepth );
        branchNode->mChildren[3] = BuildNode( lowerRight, points, curDepth + 1, maxDepth );

        // If all of the children are empty delete node.
        if (branchNode->mChildren[0] == nullptr &&
            branchNode->mChildren[1] == nullptr &&
            branchNode->mChildren[2] == nullptr &&
            branchNode->mChildren[3] == nullptr)
        {
            delete branchNode;
            branchNode = nullptr;
        }

        return branchNode;
    }
    else
    {
        // Alloc leaf node.
        LeafNode* leafNode = new LeafNode( bounds );

        // Fill leaf node with rectangles that intersect it's bounds.
        for (const Point2F& p : points)
        {
            if (leafNode->mRect.Contains( p ))
            {
                leafNode->mPoints.push_back( p );
            }
        }

        // If the node is empty delete it.
        if (leafNode->mPoints.size() == 0)
        {
            delete leafNode;
            leafNode = nullptr;
        }

        return leafNode;
    }
}

//=======================================================================

void PointQuadTree::DumpNode( FILE* const fh, const Node* const node, const Node* const parent ) const
{
    if (node != nullptr)
    {
        if (!node->mIsLeaf)
        {
            // Dump branch node.
            BranchNode* const branchNode = (BranchNode*)node;
            fprintf( fh, "    n%" PRIXPTR " [style=filled,shape=record,label=\"{Branch|Rect: %.2f %.2f %.2f %.2f}\",fillcolor=\"#a0c4ff\"];\n",
                (uintptr_t)branchNode,
                branchNode->mRect.min.x,
                branchNode->mRect.min.y,
                branchNode->mRect.max.x - branchNode->mRect.min.x,
                branchNode->mRect.max.y - branchNode->mRect.min.y );
            DumpNode( fh, branchNode->mChildren[0], node );
            DumpNode( fh, branchNode->mChildren[1], node );
            DumpNode( fh, branchNode->mChildren[2], node );
            DumpNode( fh, branchNode->mChildren[3], node );
        }
        else
        {
            // Dump leaf node.
            LeafNode* const leafNode = (LeafNode*)node;
            fprintf( fh, "    n%" PRIXPTR " [style=filled,shape=record,label=\"{Leaf|Rect: %.2f %.2f %.2f %.2f|numPoints: %d}\",fillcolor=\"%s\"];\n",
                (uintptr_t)leafNode,
                leafNode->mRect.min.x,
                leafNode->mRect.min.y,
                leafNode->mRect.max.x - leafNode->mRect.min.x,
                leafNode->mRect.max.y - leafNode->mRect.min.y,
                (int)leafNode->mPoints.size(),
                leafNode->mPoints.size() == 0 ? "#ffdaa0" : "#a0ffb3" );
        }
        if (parent != nullptr)
        {
            fprintf( fh, "    n%" PRIXPTR "->n%" PRIXPTR ";\n", (uintptr_t)parent, (uintptr_t)node );
        }
    }
}

//=======================================================================

void PointQuadTree::FindPointsRecursive( std::vector<Point2F>& points, uint32_t& numTests, const RectF& rect, const Node* const node ) const
{
    // Test if node is not null.
	if (node != nullptr) {
		// Test if node rectangle overlaps with supplied rectangle (RectF::Overlaps()) increment numTests
		numTests++;
		if (node->mRect.Overlaps(rect)) { // Node overlaps:
			 // If it is a branch node: recurse to it's children.
			if (!node->mIsLeaf) {
				BranchNode *branch = (BranchNode*)node;
				FindPointsRecursive(points, numTests, rect, branch->mChildren[0]);
				FindPointsRecursive(points, numTests, rect, branch->mChildren[1]);
				FindPointsRecursive(points, numTests, rect, branch->mChildren[2]);
				FindPointsRecursive(points, numTests, rect, branch->mChildren[3]);

			}
			// If it is a leaf node: test if each point is contained in the supplied rectangle (RectF::Contains())
			else {
				LeafNode *leaf = (LeafNode*)node;
				for (int i = 0; i < leaf->mPoints.size(); i++) {
					numTests++;
					if (rect.Contains(leaf->mPoints[i])) {
						points.push_back(leaf->mPoints[i]);
					}
					//     add contained points to the points vector (points.push_back()) and increment numTests for each.
				}
			}
			
		}
	}
        
            
               
                
                
}

//=======================================================================












#if 0

void BinaryTree::DepthFirstTraversal()
{
    // Use stack to traverse Binary Tree in Depth First order.
    // Use stack to get LIFO order, so we process children before
    // siblings.
    std::stack<Node*> stack;
    
    // Start with root node.
    stack.push( mRootNode );
    
    // Loop till stack is empty.
    while (stack.size() > 0)
    {
        // Pop node off of stack.
        Node* const node = stack.top();
        stack.pop();

        // ... Evaluate node here ...

        // Push children onto stack and continue.
        if (node->mLeftNode != nullptr)
        {
            stack.push( node->mLeftNode );
        }
        if (node->mRightNode != nullptr)
        {
            stack.push( node->mRightNode );
        }
    }
}

void BinaryTree::BreadthFirstTraversal()
{
    // Use queue to traverse Binary Tree in Breadth First order.
    // Use queue to get FIFO order, so we process siblings before
    // children.
    std::queue<Node*> queue;
    
    // Start with root node.
    queue.push( mRootNode );
    
    // Loop till queue is empty.
    while (queue.size() > 0)
    {
        // Pop node off of queue.
        Node* const node = queue.front();
        queue.pop();

        // ... Evaluate node here ...

        // Push children onto queue and continue.
        if (node->mLeftNode != nullptr)
        {
            queue.push( node->mLeftNode );
        }
        if (node->mRightNode != nullptr)
        {
            queue.push( node->mRightNode );
        }
    }
}

void BinaryTree::RecursiveTraversal( Node* const node )
{
    // Recursive traversal will result in Depth First order as
    // a Stack is used by the compiler to jump to and from function
    // calls.

    // ... Evaluate node here ...

    // Recursively process children.
    if (node->mLeftNode != nullptr)
    {
        RecursiveTraversal( node->mLeftNode );
    }
    if (node->mRightNode != nullptr)
    {
        RecursiveTraversal( node->mRightNode );
    }
}

// Start recursive traversal.
RecursiveTraversal( mRootNode );

#endif



