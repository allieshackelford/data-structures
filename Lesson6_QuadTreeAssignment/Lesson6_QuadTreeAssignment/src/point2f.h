//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_POINT2F_H_
#define _VFS_POINT2F_H_

//======================================================================

class Point2F
{
public:

    inline Point2F( float const _x = 0, float const _y = 0 ):
        x( _x ),
        y( _y )
    {
    }

    // Compare points using squared length.
    inline bool operator < ( const Point2F& p ) const
    {
        return ((x * x) + (y * y)) < ((p.x * p.x) + (p.y * p.y));
    }

    // Compare points.
    inline bool operator != ( const Point2F& p ) const
    {
        return x != p.x || y != p.y;
    }

    float x;
    float y;
};

//======================================================================

#endif

//======================================================================
