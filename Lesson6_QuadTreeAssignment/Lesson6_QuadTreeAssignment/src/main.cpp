//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#include "pointquadtree.h"

//======================================================================

static float GetRandomFloat( float const min, float const max )
{
    float const r = (float)((double)rand() / (double)RAND_MAX);
    return min + (r * (max - min));
}

//======================================================================

int main()
{
    // Build vector of random points.
    uint32_t const numPoints = 1000;
    std::vector<Point2F> points( numPoints );
    RectF bounds;
    for (uint32_t i = 0; i < numPoints; i++)
    {
        Point2F const point( GetRandomFloat( 0.0f, 1000.0f ),
                             GetRandomFloat( 0.0f, 1000.0f ) );
        points[i] = point;
        bounds.Union( point );
    }

    // Build quadtree.
    PointQuadTree quadTree;
    quadTree.Build( bounds, points, 4 );

    // Dump tree in Graphiz dot format.
    quadTree.Dump( "quadtree.dot" );

    // Test tree.
    uint32_t const numTests = 10;
    for (uint32_t i = 0; i < numTests; i++)
    {
        // Build test rectangle.
        printf( "=============================================\n");
        Point2F const min( GetRandomFloat( 0.0f, 900.0f ),
                           GetRandomFloat( 0.0f, 900.0f ) );
        Point2F const max( min.x + 100.0f,
                           min.y + 100.0f );
        RectF const testRect( min, max );
        printf( "= Testing QuadTree with Rect: x: %.2f, y: %.2f, w: %.2f, h: %.2f\n",
            min.x, min.y, max.x - min.x, max.y - min.y );

        // Use quadtree to find points contained in rectangle.
        std::vector<Point2F> containedPoints;
        uint32_t const numTests = quadTree.FindPoints( containedPoints, testRect );
        std::sort( containedPoints.begin(), containedPoints.end() );
        printf( "= QuadTree: Found: %d points in: %d tests.\n", containedPoints.size(), numTests );

        // Find points using brute force.
        std::vector<Point2F> refContainedPoints;
        for (const Point2F& point : points)
        {
            if (testRect.Contains( point ))
            {
                refContainedPoints.push_back( point );
            }
        }
        std::sort( refContainedPoints.begin(), refContainedPoints.end() );
        printf( "= Vector: Found: %d points in: %d tests.\n", refContainedPoints.size(), points.size() );

        // Compare vectors.
        if (containedPoints.size() != refContainedPoints.size())
        {
            printf( "= Error: expected: %d points, got: %d\n", refContainedPoints.size(), containedPoints.size() );
        }
        else
        {
            for (uint32_t i = 0; i < refContainedPoints.size(); i++)
            {
                if (containedPoints[i] != refContainedPoints[i])
                {
                    printf( "= Error: point %d doesn't match, expected: x: %.2f, y: %.2f, got: x: %.2f, y: %.2f\n",
                        i, refContainedPoints[i].x, refContainedPoints[i].y, containedPoints[i].x, containedPoints[i].y );
                }
            }
        }
    }

    system( "pause" );

    return 0;
}

//======================================================================
