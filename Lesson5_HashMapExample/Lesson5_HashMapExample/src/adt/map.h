//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_MAP_H_
#define _VFS_MAP_H_

//======================================================================

#include <stdint.h>

//======================================================================

template <class K, class V>
class Map
{
public:

    // Virtual Destructor, always need this for base classes.
    virtual ~Map() {}

    // Inserts an entry.
    virtual bool Insert( const K& key, const V& value ) = 0;

    // Removes an entry.
    virtual bool Remove( const K& key ) = 0;

    // Gets the entry with the supplied key.
    virtual V* At( const K& key ) = 0;

    // Return the number of entries in map.
    virtual uint32_t Size() const = 0;
};

//======================================================================

#endif

//======================================================================
