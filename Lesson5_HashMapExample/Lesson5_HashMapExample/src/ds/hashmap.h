//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#if !defined _VFS_HASHMAP_H_
#define _VFS_HASHMAP_H_

//======================================================================

#include "adt/map.h"
#include <vector>
#include <assert.h>

//======================================================================

template <class K,class V>
class HashMap: public Map<K,V>
{
public:

    //------------------------------------------------------------------
    // Hashing function type.
    using HashFunc = uint32_t(*)( const K& key );

    //------------------------------------------------------------------
    // HashMap constructor, takes a Hash function pointer and the
    // hash table size.
    HashMap( HashFunc const hashFunc, uint32_t const tableSize ):
        mHashFunc( hashFunc ),
        mSize( 0 ),
        mCollisions( 0 )
    {
        assert( tableSize > 0 );
        if (tableSize > 0)
        {
            mTable.resize( tableSize );
        }
    }

    //------------------------------------------------------------------
    // Destructor.
    virtual ~HashMap()
    {
    }

    //------------------------------------------------------------------
    // Inserts an entry.
    virtual bool Insert( const K& key, const V& value ) override
    {
        // Hash key to integer key.
        uint32_t const hash = mHashFunc( key );

        // Calculate hash table bin index.
        uint32_t const tableIndex = hash % mTable.size();

        // Make sure entry doesn't already exist.
        for (const Entry& entry : mTable[tableIndex])
        {
            if (entry.mHash == hash)
            {
                if (entry.mKey == key)
                {
                    // Entry is already in map, return false.
                    return false;
                }
                // Track hashing collisions.
                mCollisions++;
            }
        }

        // Add entry.
        mTable[tableIndex].emplace_back( Entry( key, value, hash ) );
        mSize++;

        return true;
    }

    //------------------------------------------------------------------
    // Removes an entry.
    virtual bool Remove( const K& key ) override
    {
        // Hash key to integer key.
        uint32_t const hash = mHashFunc( key );

        // Calculate hash table bin index.
        uint32_t const tableIndex = hash % mTable.size();

        // Find and remove entry.
        for (auto it = mTable[tableIndex].begin(), end = mTable[tableIndex].end();
             it != end;
             ++it)
        {
            const Entry& entry = *it;
            if (entry.mHash == hash && entry.mKey == key)
            {
                mTable[tableIndex].erase( it );
                mSize--;
                return true;
            }
        }
        return false;
    }

    //------------------------------------------------------------------
    // Gets the entry with the supplied key.
    virtual V* At( const K& key ) override
    {
        // Hash key to integer key.
        uint32_t const hash = mHashFunc( key );

        // Calculate hash table bin index.
        uint32_t const tableIndex = hash % mTable.size();

        // Find entry.
        for (Entry& entry : mTable[tableIndex])
        {
            if (entry.mHash == hash && entry.mKey == key)
            {
                return &(entry.mValue);
            }
        }
        return nullptr;
    }

    //------------------------------------------------------------------
    // Return the number of entries in map.
    virtual uint32_t Size() const override
    {
        return mSize;
    }

    //------------------------------------------------------------------
    // Returns the number of collisions that have occurred.
    uint32_t Collisions() const
    {
        return mCollisions;
    }

    //------------------------------------------------------------------
    // Dump CSV file with table bin counts.
    bool DumpCSV( const char* const fileName ) const
    {
        FILE* fh = fopen( fileName, "wb" );
        if (fh != nullptr)
        {
            fprintf( fh, "Count\n" );
            for (uint32_t i = 0; i < (uint32_t)mTable.size(); i++)
            {
                fprintf( fh, "%d\n", (int)mTable[i].size() );
            }
            fclose( fh );
            return true;
        }
        return false;
    }

    //------------------------------------------------------------------
    // Hack to iterate.
    bool Walk( K& key, V& value, uint32_t& bin, uint32_t& offs )
    {
        if (bin >= mTable.size())
        {
            return false;
        }
        while (offs >= mTable[bin].size())
        {
            bin++;
            offs = 0;
            if (bin >= mTable.size())
            {
                return false;
            }
        }
        key = mTable[bin][offs].mKey;
        value = mTable[bin][offs].mValue;
        offs++;
        return true;
    }

private:

    struct Entry
    {
        Entry( const K& key, const V& value, uint32_t const hash ):
            mKey( key ),
            mValue( value ),
            mHash( hash )
        {
        }

        K mKey;
        V mValue;
        uint32_t mHash;
    };

    std::vector< std::vector< Entry > > mTable;
    HashFunc mHashFunc;
    uint32_t mSize;
    uint32_t mCollisions;
};

//======================================================================

#endif

//======================================================================
