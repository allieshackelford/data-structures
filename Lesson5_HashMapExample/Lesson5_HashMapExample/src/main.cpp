//======================================================================
// VFS: Data Strucrtures And Algorithms
//======================================================================

#include "ds/hashmap.h"
#include "MurmurHash3.h"
#include <stdlib.h>
#include <fstream>
#include <iostream>

//=======================================================================

// Hashing function used by HashMap, hashes string to 32bit integer.
// There a couple different approaches here to profile collision and
// distribution.

//#define SUM_HASH
//#define STL_HASH
#define JAVA_HASH
//#define MURMUR_HASH

uint32_t HashFunc( const std::string& str )
{
    uint32_t key = 5381;

// Naive sum hash.
#if defined SUM_HASH
    for (char const c : str)
    {
        key += c;
    }

// Hashing algorithm used by STL (C++ std namespace).
#elif defined STL_HASH
    key = (uint32_t)std::hash<std::string>{}( str );

// Hashing algorithm used by Java.
#elif defined JAVA_HASH
    for (char const c : str)
    {
        key = 31 * key + c;
    }

// Murmur hash.
#elif defined MURMUR_HASH
    MurmurHash3_x86_32( str.c_str(), (int)str.length(), key, &key );
#endif

    return key;
}

//======================================================================

int main()
{
    // Use HashMap to count the number of times a word appears in the
    // KJV of the bible.
    HashMap<std::string,uint32_t> hashMap( HashFunc, 1024 );
    uint32_t numWords = 0;
    std::ifstream ifh;
    char buf[256];
    ifh.open( "kjv_words.txt", std::ifstream::in );
    if (ifh.is_open())
    {
        while (ifh.good())
        {
            // Read word.
            ifh.getline( buf, 256, ';' );

            // Search map for word. This will hash the word and use
            // the hash key as an index into the hash table. If the
            // the entry doesn't exist At will return null.
            std::string const str( buf );
            uint32_t* const value = hashMap.At( str );
            if (value == nullptr)
            {
                // Add word, set it's count to 1.
                hashMap.Insert( str, 1 );
            }
            else
            {
                // Found the word, increment it's count.
                (*value)++;
            }

            // Increment word count.
            numWords++;
        }
        ifh.close();
    }

    // Dump info, DumpCSV will dump the bin usage numbers that demonstrate
    // 
    printf( "%d Collisions for %d words, %d unique words.\n", hashMap.Collisions(), numWords, hashMap.Size() );
    hashMap.DumpCSV( "hashmap.csv" );

    // Dump contents of map: word count of all the words in the KJV of the
    // bible.
    FILE* const ofh = fopen( "results.csv", "wb" );
    if (ofh != nullptr)
    {
        fprintf( ofh, "word,count\n" );
        uint32_t bin = 0;
        uint32_t offs = 0;
        std::string key;
        uint32_t val;
        while (hashMap.Walk( key, val, bin, offs ))
        {
            fprintf( ofh, "%s,%d\n", key.c_str(), val );
        }
        fclose( ofh );
    }

    system( "pause" );

    return 0;
}

//======================================================================
