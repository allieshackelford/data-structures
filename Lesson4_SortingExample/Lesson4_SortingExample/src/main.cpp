//======================================================================
// VFS: Data Structures And Algorithms
//======================================================================

#include <stdint.h>
#include <vector>
#include <algorithm>

//======================================================================

struct RenderableObject
{
    RenderableObject( uint32_t const name, uint32_t const materialHandle, uint32_t const meshHandle ):
        mName( name ),
        mMaterialHandle( materialHandle ),
        mMeshHandle( meshHandle )
    {
    }

    int compare( const RenderableObject& b ) const
    {
        if (mMaterialHandle != b.mMaterialHandle)
            return (mMaterialHandle < b.mMaterialHandle) ? -1 : 1;
        if (mMeshHandle != b.mMeshHandle)
            return (mMeshHandle < b.mMeshHandle) ? -1 : 1;
        return 0;
    }

    bool operator <( const RenderableObject& b ) const
    {
        return compare( b ) < 0;
    }

    void dump() const
    {
        printf( "name: %d, materialHandle: %d, meshHandle: %d\n", mName, mMaterialHandle, mMeshHandle );
    }

    uint32_t mName;
    uint32_t mMaterialHandle;
    uint32_t mMeshHandle;
};

//======================================================================

int cmpfunc( const void* a, const void* b )
{
    return (*(RenderableObject*)a).compare( (*(RenderableObject*)b) );
}

//======================================================================

int main()
{
    uint32_t const numObjects = 100;
    std::vector<RenderableObject> objects;
    for (uint32_t i = 0; i < numObjects; i++)
    {
        objects.push_back( RenderableObject( i + 1, (rand() % 10) + 1, (rand() % 10) + 1 ) );
    }
    std::sort( objects.begin(), objects.end() );
    //qsort( &(objects[0]), objects.size(), sizeof( RenderableObject ), cmpfunc );
    for (auto& obj : objects)
    {
        obj.dump();
    }
    system( "pause" );
    return 0;
}

//======================================================================

